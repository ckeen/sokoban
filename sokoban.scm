(use srfi-1 doodle matchable)


;; So we need the following elements
;; # - Wall
;; o - Box
;; . - Target spot
;; * - Box in target spot
;; @ - Box pushing person
(define items '((wall . #\#)
                (box . #\$)
                (spot . #\.)
                (free . #\space)
                (box-in-spot . #\*)
                (pusher-in-spot . #\+)
                (pusher . #\@)))

;; Game state consists of
;; board
;; player position
;; box positions
;; target positions
;; Coordinates are just a list of X and Y coordinates
(define-record game board player boxes spots)

(define levels (with-input-from-file "levels" read))

(define (read-board b)
  (let ((bl (map (lambda (r) (string->list r)) b))
        (g (make-game #f '(0 . 0) '() '())))
    (game-board-set! g bl)
    (fold (lambda (l y)
            (fold (lambda (i x)
                    (case i
                      ((#\$)
                       (game-boxes-set! g (cons (cons x y) (game-boxes g))))
                      ((#\.)
                       (game-spots-set! g (cons (cons x y) (game-spots g))))
                      ((#\@)
                       (game-player-set! g (cons x y)))
                      ((#\*)
                       (game-boxes-set! g (cons (cons x y) (game-boxes g)))
                       (game-spots-set! g (cons (cons x y) (game-spots g))))
                      (else (void)))
                    (add1 x)) 0 l)
            (add1 y))
          0 bl)
    g))

;; Get a tile off the game board
(define (get-tile board x y)
   (and
    (<= 0 y)
    (<= 0 x)
    (< y (length board))
    (< x (length (list-ref board y)))
    (list-ref (list-ref board y) x)))

;; Also set a tile on the board
(define (set-tile! board x y tile)
  (and
   (<= 0 y)
   (<= 0 x)
   (< y (length board))
   (< x (length (list-ref board y)))
   (set! (list-ref (list-ref board y) x) tile)))

(define (equal-tile? board x y tile)
  (equal? (get-tile board x y)
          (alist-ref tile items equal?)))

;; Rules of the game:
;; - Only one box can be pushed at a time.
;; - A box cannot be pulled.
;; - The player cannot walk through boxes or walls.
;; - The puzzle is solved when all boxes are located at storage locations.


(define up '(0 . -1))
(define down '(0 . +1))
(define left '(-1 . 0))
(define right '(+1 . 0))

;; Checks whether a move which is a new position coordinate
;; *operation* (differences in coordinates) given the current game
;; state
(define (legal-move? game move)
  (let* ((player (game-player game))
         (px (car player))
         (py (cdr player))
         (b (game-board game))
         (newx (+ px (car move)))
         (newy (+ py (cdr move))))
    (define (within-board?)
      (and (<= 0 newy)
           (< newy (length b))
           (<= 0 newx)
           (< newx (length (list-ref b newy)))))
    (define (wall-ahead?)
      (equal-tile? b newx newy 'wall))
    (define (box-ahead? #!optional (x newx) (y newy))
      (member (cons x y) (game-boxes game)))
    (define (one-box-at-a-time?)
      (and (box-ahead?)
           (not (box-ahead?
                 (+ newx (car move))
                 (+ newy (cdr move))))
           (or
            (equal-tile? b
                         (+ newx (car move))
                         (+ newy (cdr move))
                         'free)
            (equal-tile? b
                         (+ newx (car move))
                         (+ newy (cdr move))
                         'spot))))
    (and (within-board?)
         (not (wall-ahead?))
         (if (box-ahead?)
             (one-box-at-a-time?)
             #t))))

(define (move! game move)
  (define (update-box! game x y newx newy)
    (let ((bs (game-boxes game)))
      (when (member (cons x y) (game-spots game))
            (set-tile! (game-board game) x y (alist-ref 'spot items)))
      (if (member (cons newx newy) (game-spots game))
            (set-tile! (game-board game) newx newy (alist-ref 'box-in-spot items))
            (set-tile! (game-board game) newx newy (alist-ref 'box items)))
      (game-boxes-set!
       game
       (cons (cons newx newy)
             (remove (cut equal? (cons x y) <>)
                     (game-boxes game))))))
  (let* ((player (game-player game))
         (px (car player))
         (py (cdr player))
         (newx (+ px (car move)))
         (newy (+ py (cdr move)))
         (board (game-board game)))

    (when (legal-move? game move)
          (let* ((t (get-tile board newx newy))
                 (tn (alist-ref (map (lambda (a) (cons (cdr a) (car a))) items) t)))
            (cond ((member (cons newx newy) (game-boxes game))
                   (update-box! game
                                newx
                                newy
                                (+ newx (car move))
                                (+ newy (cdr move)))))
            (if (member (cons px py) (game-spots game))
                  (set-tile! (game-board game) px py #\.)
                  (set-tile! (game-board game) px py #\space))
            (cond ((member (cons newx newy) (game-spots game))
                   (set-tile! (game-board game) newx newy #\+)
                   (game-player-set! game (cons newx newy)))
                  (else
                   (set-tile! (game-board game) newx newy #\@)
                   (game-player-set! game (cons newx newy))))))))

;; A game is done if the boxes list is equal to our spot lists
(define (level-complete? game)
  (lset= equal? (game-spots game)
          (game-boxes game)))

(define (print-board board)
  (for-each (lambda (l) (print (list->string l))) board))

(new-doodle fullscreen: #t width: 1440 height: 900)

(define *terrain-x-offset* 0)
(define *terrain-y-offset* -20)
(define *tile-width* 60)
(define *tile-height* 50)
(define *terrain-scale* 0.6)
(define *box-scale* 0.4)

(define-resource 'wall #:image "assets/Stone Block Tall.png" *terrain-x-offset* *terrain-y-offset* *terrain-scale*)
(define-resource 'pusher #:image "assets/Character Pink Girl.png" *terrain-x-offset* -50 0.8)
(define-resource 'box #:image "assets/Gem Orange.png" 0 -30 *box-scale*)
(define-resource 'box-in-spot #:image "assets/Gem Green.png" 0 -30 *box-scale*)
(define-resource 'free #:image "assets/Wood Block.png" *terrain-x-offset* *terrain-y-offset* *terrain-scale*)
(define-resource 'spot #:image "assets/Dirt Block.png" *terrain-x-offset* *terrain-y-offset* *terrain-scale*)

(define *b* #f)

(define (draw-board game)
  (fold
   (lambda (r y)
     (fold
      (lambda (t x)
        (let* ((t (alist-ref (get-tile (game-board game) x y)
                            (map (lambda (a) (cons (cdr a) (car a)))
                                 items)
                            equal?)))
          (cond ((equal? t 'pusher-in-spot)
                 (blit-image 'spot (* x *tile-width*) (* *tile-height* y))
                 (blit-image 'pusher (* x *tile-width*) (* *tile-height* y)))
                ((equal? t 'box-in-spot)
                 (blit-image 'spot (* x *tile-width*) (* *tile-height* y))
                 (blit-image 'box-in-spot (* x *tile-width*) (* *tile-height* y)))
                (else
                 (blit-image 'free (* x *tile-width*) (* *tile-height* y))
                 (blit-image t (* x *tile-width*) (* *tile-height* y)))))
        (add1 x))
      0
      r)
     (add1 y))
   0
   (game-board game)))

(define (draw-walls game)
  (fold
   (lambda (r y)
     (fold
      (lambda (t x)
        (when (equal? (get-tile (game-board game) x y) #\#)
              (blit-image 'wall (* x *tile-width*) (* *tile-height* y)))
        (add1 x))
      0
      r)
     (add1 y))
   0
   (game-board game)))

(define (draw-boxes game)
  (for-each
   (lambda (b)
     (unless (member b (game-spots game) equal?)
             (blit-image 'box  (* *tile-width* (car b)) (* *tile-height* (cdr b)))))
   (game-boxes game)))

(define (draw-spots game)
  (for-each
   (lambda (b)
     (cond ((member b (game-boxes game) equal?)
             (blit-image 'spot (* *tile-width* (car b)) (* *tile-height* (cdr b)))
             (blit-image 'box-in-spot (* *tile-width* (car b)) (* *tile-height* (cdr b))))
           (else (blit-image 'spot (* *tile-width* (car b)) (* *tile-height* (cdr b))))))
   (sort
    (game-spots game)
    (lambda (a b)
      (< (cdr a) (cdr b))))))



(define (draw-player game)
  (blit-image 'pusher (* *tile-width* (car (game-player game)))
              (* *tile-height* (cdr (game-player game)))))

(define (next-level!)
  (set! *b* (read-board (cdar levels)))
  (set! *current* (cdar levels))
  (set! levels (cdr levels)))

(world-inits
 (lambda ()
   (next-level!)))

(world-changes
 (lambda (events dt quit)
   (for-each
    (lambda (e)
      (match e
             (('key 'pressed 'up) (move! *b* up))
             (('key 'pressed 'down) (move! *b* down))
             (('key 'pressed 'left) (move! *b* left))
             (('key 'pressed 'right) (move! *b* right))
             (('key 'pressed '#\r) (set! *b* (read-board *current*)))
             (('key 'pressed '#\q) (quit 'done))
             (else (void))))
    events)
   (clear-screen)
   (draw-board *b*)

   (show!)
   (when (level-complete? *b*)
         (next-level!))))

(run-event-loop run-in-background: #t)
